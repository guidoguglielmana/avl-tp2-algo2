build:
	gcc *.c -o avl -g -std=c99 -Wall -Wconversion -Wtype-limits -pedantic -Werror -O0

run: build
	./avl
