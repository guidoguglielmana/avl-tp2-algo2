#ifndef __AVL_H__
#define __AVL_H__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

typedef struct nodo{
    void* clave;
    struct nodo* izquierda;
    struct nodo* derecha;
    int altura;
} nodo_t;


int maximo(int a, int b);

int altura_nodo(nodo_t* nodo);

nodo_t* rotar_derecha(nodo_t* nodo);

nodo_t* rotar_izquierda(nodo_t* nodo);

int obtener_balance(nodo_t* nodo);

nodo_t* insertar(nodo_t* nodo, void* clave);

nodo_t* borrar(nodo_t* nodo, void* clave);


#endif /* __AVL_H__ */





