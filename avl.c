// C program to delete a node from AVL Tree 
#include "avl.h"
#define COUNT 10


int altura_nodo(nodo_t* nodo){ 
	if (nodo == NULL) 
		return 0; 
	return nodo->altura; 
} 

int maximo(int a, int b){ 
	return (a > b)? a : b; 
} 

/* Helper function that allocates a new node with the given key and 
	NULL left and right pointers. */
nodo_t* crear_nodo(void* clave){ 
	nodo_t* nodo = (nodo_t*)malloc(sizeof(nodo_t));
	if(nodo == NULL){
		return NULL;
	}
	nodo->clave = clave; 
	nodo->izquierda = NULL; 
	nodo->derecha = NULL; 
	nodo->altura = 1; // new nodo is initially added at leaf 
	return(nodo); 
} 

// A utility function to right rotate subtree rooted with y 
// See the diagram given above. 
nodo_t* rotar_derecha(nodo_t* nodo){ 
	nodo_t* aux = nodo->izquierda; 
	nodo_t* rotar = aux->derecha; 

	// Perform rotation 
	aux->derecha = nodo; 
	nodo->izquierda = rotar; 

	// Update heights 
	nodo->altura = maximo(altura_nodo(nodo->izquierda), altura_nodo(nodo->derecha))+1; 
	aux->altura = maximo(altura_nodo(aux->izquierda), altura_nodo(aux->derecha))+1; 

	// Return new root 
	return aux; 
} 

// A utility function to left rotate subtree rooted with x 
// See the diagram given above. 
nodo_t* rotar_izquierda(nodo_t* nodo){
	nodo_t* aux = nodo->derecha; 
	nodo_t* rotar = aux->izquierda; 

	// Perform rotation 
	aux->izquierda = nodo; 
	nodo->derecha = rotar; 

	// Update heights 
	nodo->altura = maximo(altura_nodo(nodo->izquierda), altura_nodo(nodo->derecha))+1; 
	aux->altura = maximo(altura_nodo(aux->izquierda), altura_nodo(aux->derecha))+1; 

	// Return new root 
	return aux; 
} 

// Get Balance factor of node N 
int obtener_balance(nodo_t* nodo) { 
	if (nodo == NULL) 
		return 0; 
	return (altura_nodo(nodo->izquierda) - altura_nodo(nodo->derecha)); 
}

nodo_t* insertar(nodo_t* nodo, void* clave){ 
	/* 1. Perform the normal BST rotation */
	if (nodo == NULL) 
		return(crear_nodo(clave)); 

	if (clave < nodo->clave) 
		nodo->izquierda = insertar(nodo->izquierda, clave); 
	else if (clave > nodo->clave) 
		nodo->derecha = insertar(nodo->derecha, clave); 
	else // Equal claves not allowed 
		return nodo; 

	/* 2. Update height of this ancestor nodo */
	nodo->altura = 1 + maximo(altura_nodo(nodo->izquierda), altura_nodo(nodo->derecha)); 

	/* 3. Get the balance factor of this ancestor 
		nodo to check whether this nodo became 
		unbalanced */
	int balance = obtener_balance(nodo); 

	// If this nodo becomes unbalanced, then there are 4 cases 

	// izquierda izquierda Case 
	if (balance > 1 && clave < nodo->izquierda->clave) 
		return rotar_derecha(nodo); 

	// derecha derecha Case 
	if (balance < -1 && clave > nodo->derecha->clave) 
		return rotar_izquierda(nodo); 

	// Left derecha Case 
	if (balance > 1 && clave > nodo->izquierda->clave) 
	{ 
		nodo->izquierda = rotar_izquierda(nodo->izquierda); 
		return rotar_derecha(nodo); 
	} 

	// derecha Left Case 
	if (balance < -1 && clave < nodo->derecha->clave) 
	{ 
		nodo->derecha = rotar_derecha(nodo->derecha); 
		return rotar_izquierda(nodo); 
	} 

	/* return the (unchanged) nodo pointer */
	return nodo; 
} 

/* Given a non-empty binary search tree, return the 
node with minimum key value found in that tree. 
Note that the entire tree does not need to be 
searched. */
nodo_t* minValueNode(nodo_t* nodo) 
{ 
	nodo_t* current = nodo; 

	/* loop down to find the leftmost leaf */
	while (current->izquierda != NULL) 
		current = current->izquierda; 

	return current; 
} 

// Recursive function to delete a node with given key 
// from subtree with given root. It returns root of 
// the modified subtree. 
nodo_t* borrar(nodo_t* nodo, void* clave){
	// STEP 1: PERFORM STANDARD BST DELETE 

	if (nodo == NULL) 
		return nodo; 

	// If the key to be deleted is smaller than the 
	// root's key, then it lies in left subtree 
	if ( clave < nodo->clave ) 
		nodo->izquierda = borrar(nodo->izquierda, clave); 

	// If the key to be deleted is greater than the 
	// root's key, then it lies in derecha subtree 
	else if( clave > nodo->clave ) 
		nodo->derecha = borrar(nodo->derecha, clave); 

	// if key is same as root's key, then This is 
	// the node to be deleted 
	else
	{ 
		// node with only one child or no child 
		if( (nodo->izquierda == NULL) || (nodo->derecha == NULL) ) 
		{ 
			nodo_t* temp = nodo->izquierda ? nodo->izquierda : 
											nodo->derecha; 

			// No child case 
			if (temp == NULL) 
			{ 
				temp = nodo; 
				nodo = NULL; 
			} 
			else // One child case 
			*nodo = *temp; // Copy the contents of 
							// the non-empty child 
			free(temp); 
		} 
		else
		{ 
			// node with two children: Get the inorder 
			// successor (smallest in the right subtree) 
			nodo_t* temp = minValueNode(nodo->derecha); 

			// Copy the inorder successor's data to this node 
			nodo->clave = temp->clave; 

			// Delete the inorder successor 
			nodo->derecha = borrar(nodo->derecha, temp->clave); 
		} 
	} 

	// If the tree had only one node then return 
	if (nodo == NULL) 
	return nodo; 

	// STEP 2: UPDATE HEIGHT OF THE CURRENT NODE 
	nodo->altura = 1 + maximo(altura_nodo(nodo->izquierda), 
						altura_nodo(nodo->derecha)); 

	printf("la altura es %i", nodo->altura);

	// STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to 
	// check whether this node became unbalanced) 
	int balance = obtener_balance(nodo); 

	// If this node becomes unbalanced, then there are 4 cases 

	// Left Left Case 
	if (balance > 1 && obtener_balance(nodo->izquierda) >= 0) 
		return rotar_derecha(nodo); 

	// izquierda Right Case 
	if (balance > 1 && obtener_balance(nodo->izquierda) < 0) 
	{ 
		nodo->izquierda = rotar_izquierda(nodo->izquierda); 
		return rotar_derecha(nodo); 
	} 

	// Right Right Case 
	if (balance < -1 && obtener_balance(nodo->derecha) <= 0) 
		return rotar_izquierda(nodo); 

	// derecha izquierda Case 
	if (balance < -1 && obtener_balance(nodo->derecha) > 0) 
	{ 
		nodo->derecha = rotar_derecha(nodo->derecha); 
		return rotar_izquierda(nodo); 
	} 

	return nodo; 
} 

// A utility function to print preorder traversal of 
// the tree. 
// The function also prints height of every node 
void preOrder(nodo_t* root) 
{ 
	if(root != NULL) 
	{ 
		printf("%d ", (int)root->clave); 
		preOrder(root->izquierda); 
		preOrder(root->derecha); 
	} 
} 

void print2DUtil(nodo_t* root, int space) 
{ 
    // Base case 
    if (root == NULL) 
        return; 
  
    // Increase distance between levels 
    space += COUNT; 
  
    // Process right child first 
    print2DUtil(root->derecha, space); 
  
    // Print current node after space 
    // count 
    printf("\n"); 
    for (int i = COUNT; i < space; i++) 
        printf(" "); 
    printf("%d\n", (int)root->clave); 
  
    // Process left child 
    print2DUtil(root->izquierda, space); 
} 
  
// Wrapper over print2DUtil() 
void print2D(nodo_t *root) 
{ 
   // Pass initial space count as 0 
   print2DUtil(root, 0); 
} 

/* Driver program to test above function*/
int main() 
{ 
nodo_t* nodo = NULL; 

/* Constructing tree given in the above figure */
	nodo = insertar(nodo, (int*)9); 
	nodo = insertar(nodo, (int*)5); 
	nodo = insertar(nodo, (int*)10); 
	nodo = insertar(nodo, (int*)0); 
	nodo = insertar(nodo, (int*)6); 
	nodo = insertar(nodo, (int*)11); 
	nodo = insertar(nodo, (int*)-1); 
	nodo = insertar(nodo, (int*)1); 
	nodo = insertar(nodo, (int*)2); 

	/* The constructed AVL Tree would be 
			9 
		/ \ 
		1 10 
		/ \	 \ 
	0 5	 11 
	/ / \ 
	-1 2 6 
	*/

	printf("Preorder traversal of the constructed AVL "
		"tree is \n"); 
	preOrder(nodo); 

	nodo = borrar(nodo, (int*)10); 

	/* The AVL Tree after deletion of 10 
			1 
		/ \ 
		0 9 
		/	 / \ 
	-1 5	 11 
		/ \ 
		2 6 
	*/

	printf("\nPreorder traversal after deletion of 10 \n"); 
	preOrder(nodo); 
	print2D(nodo);

	return 0; 
} 
